import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import {OneSignal, OSNotificationPayload} from "@ionic-native/onesignal";


const sender_id = '778100826841'; //Google Proyect Sender ID
const oneSignalAppId = '47ffb9b0-7e9f-45cd-b2fc-300b6a43794d'; //One Signal API ID

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;


  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private oneSignal: OneSignal)
  {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Home', component: HomePage},
      {title: 'List', component: ListPage}
    ];


    this.oneSignal.startInit(oneSignalAppId, sender_id);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationReceived().subscribe(data =>
      this.onPushReceived(data.payload));
    this.oneSignal.handleNotificationOpened().subscribe(data =>
      this.onPushOpened(data.notification.payload));
    this.oneSignal.endInit();

  }

  private onPushReceived(payload: OSNotificationPayload) {

    console.info('Push recevied:' + payload.body);
    alert('Push recevied:' + payload.body);
  }

  private onPushOpened(payload: OSNotificationPayload) {
    console.info('Push opened: ' + payload.body);
    alert('Push opened: ' + payload.body);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
